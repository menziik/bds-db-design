SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE TABLE IF NOT EXISTS `customer` (
  `customer_id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `middle_name` VARCHAR(45) NULL,
  `surname` VARCHAR(45) NOT NULL,
  `passport_number` INT NOT NULL,
  `year_of_birth` INT NOT NULL,
  PRIMARY KEY (`customer_id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `address` (
  `address_id` INT NOT NULL AUTO_INCREMENT,
  `city` VARCHAR(45) NOT NULL,
  `street` VARCHAR(45) NULL,
  `house_number` VARCHAR(45) NOT NULL,
  `zip` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`address_id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `customer_has_address` (
  `address_type` VARCHAR(45) NOT NULL,
  `customer_id` INT NOT NULL,
  `address_id` INT NOT NULL,
  PRIMARY KEY (`customer_id`, `address_id`),
  INDEX `fk_customer_has_address_address1_idx` (`address_id` ASC),
  CONSTRAINT `fk_customer_has_address_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_has_address_address1`
    FOREIGN KEY (`address_id`)
    REFERENCES `address` (`address_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `contact` (
  `contact_id` INT NOT NULL AUTO_INCREMENT,
  `contact_type` VARCHAR(45) NOT NULL,
  `contact_address` VARCHAR(45) NOT NULL,
  `customer_id` INT NOT NULL,
  PRIMARY KEY (`contact_id`, `customer_id`),
  INDEX `fk_contact_customer1_idx` (`customer_id` ASC),
  CONSTRAINT `fk_contact_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `car` (
  `car_id` INT NOT NULL AUTO_INCREMENT,
  `car_brand` VARCHAR(45) NOT NULL,
  `car_model` VARCHAR(45) NOT NULL,
  `car_year` INT NOT NULL,
  `car_price` INT NOT NULL,
  PRIMARY KEY (`car_id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `rent_info` (
  `rent_id` INT NOT NULL AUTO_INCREMENT,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  `usage` VARCHAR(45) NOT NULL,
  `note` VARCHAR(45) NULL,
  `car_id` INT NOT NULL,
  PRIMARY KEY (`rent_id`, `car_id`),
  INDEX `fk_rent_info_car1_idx` (`car_id` ASC),
  CONSTRAINT `fk_rent_info_car1`
    FOREIGN KEY (`car_id`)
    REFERENCES `car` (`car_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `passenger` (
  `passenger_id` INT NOT NULL AUTO_INCREMENT,
  `passenger_name` VARCHAR(45) NOT NULL,
  `passenger_middle_name` VARCHAR(45) NULL,
  `passenger_surname` VARCHAR(45) NOT NULL,
  `passenger_birth` DATE NOT NULL,
  `customer_id` INT NOT NULL,
  PRIMARY KEY (`passenger_id`),
  INDEX `fk_passenger_customer1_idx` (`customer_id` ASC),
  CONSTRAINT `fk_passenger_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `license` (
  `license_id` INT NOT NULL AUTO_INCREMENT,
  `licence_type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`license_id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `customer_has_license` (
  `license_id` INT NOT NULL,
  `customer_id` INT NOT NULL,
  PRIMARY KEY (`license_id`, `customer_id`),
  INDEX `fk_customer_has_licence_customer1_idx` (`customer_id` ASC),
  CONSTRAINT `fk_customer_has_licence_licence1`
    FOREIGN KEY (`license_id`)
    REFERENCES `license` (`license_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_has_licence_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `equipment` (
  `equipment_id` INT NOT NULL AUTO_INCREMENT,
  `equipment_type` VARCHAR(45) NOT NULL,
  `customer_id` INT NOT NULL,
  PRIMARY KEY (`equipment_id`),
  INDEX `fk_equipment_customer1_idx` (`customer_id` ASC),
  CONSTRAINT `fk_equipment_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `health` (
  `health_id` INT NOT NULL AUTO_INCREMENT,
  `body_disfunction` VARCHAR(45) NULL,
  `mental_disfunction` VARCHAR(45) NULL,
  PRIMARY KEY (`health_id`))
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `customer_has_health` (
  `customer_id` INT NOT NULL,
  `health_id` INT NOT NULL,
  INDEX `fk_customer_has_health_customer1_idx` (`customer_id` ASC),
  INDEX `fk_customer_has_health_health1_idx` (`health_id` ASC),
  CONSTRAINT `customer_id`
    FOREIGN KEY (`customer_id`)
    REFERENCES `customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_has_health_health1`
    FOREIGN KEY (`health_id`)
    REFERENCES `health` (`health_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `customer_has_rent` (
  `customer_id` INT NOT NULL,
  `rent_id` INT NOT NULL,
  PRIMARY KEY (`customer_id`, `rent_id`),
  INDEX `fk_customer_has_rent_info_rent_info1_idx` (`rent_id` ASC),
  INDEX `fk_customer_has_rent_info_customer1_idx` (`customer_id` ASC),
  CONSTRAINT `fk_customer_has_rent_info_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `customer` (`customer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_customer_has_rent_info_rent_info1`
    FOREIGN KEY (`rent_id`)
    REFERENCES `rent_info` (`rent_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;