--customers
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Martin', 'Děcký', 54652656, 2000);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Šimon', 'Čížek', 45456424, 1999);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Sára', 'Březinová', 89746545, 2000);
INSERT INTO customer(first_name, middle_name, surname, passport_number, year_of_birth) VALUES ('George', 'Fion', 'Patton', 98412382, 1984);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Ulrich', 'Briter', 23154564, 1956);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Ema', 'Watson', 89755644, 1989);
INSERT INTO customer(first_name, middle_name, surname, passport_number, year_of_birth) VALUES ('Oldřich', 'Rostislav', 'Podivný', 66666666, 1966);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Petr', 'Vomáčka', 38946579, 1983);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Faiza', 'Bernal', 98564589, 1976);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Tre', 'Sheridan', 87456123, 1987);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Gurveer', 'Leblanc', 12345678, 1961);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Warren', 'Barron', 65421546, 1960);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Merryn', 'Preece', 74185296, 1998);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Minnie', 'Mayer', 13265465, 1994);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Ananya', 'Liu', 55684456, 1977);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Kaila', 'Dillard', 32155477, 1990);
INSERT INTO customer(first_name, middle_name, surname, passport_number, year_of_birth) VALUES ('Jadon', 'Hahib', 'Conrad', 88423265, 1989);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Woody', 'Myers', 74451235, 1979);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Rebekka', 'Spears', 98845622, 1986);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Lia', 'Roth', 74565215, 1972);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Matias', 'Ryan', 45678232, 1962);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Khalil', 'Weis', 65884567, 1988);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Elvis', 'Morin', 77745468, 1997);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Miya', 'Rhodes', 45675132, 1984);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Daria', 'Nairn', 11254578, 1974);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Alyssia', 'Dickinson', 87456266, 1986);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Laith', 'Dennis', 45687258, 1975);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Shoaib', 'Finley', 35469875, 1972);
INSERT INTO customer(first_name, middle_name, surname, passport_number, year_of_birth) VALUES ('Ayesha', 'Marissa', 'Massey', 89745238, 1980);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Kyle', 'Wilcox', 44456557, 1978);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Esther', 'Parsons', 55214560, 1994);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Euan', 'Greaves', 21405454, 1979);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Jess', 'Mellor', 65422354, 1972);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Jolie', 'Emerson', 45456784, 1954);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Don', 'Parkinson', 54650056, 1990);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Roshni', 'Villanueva', 78545664, 1975);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Dolcie', 'Valentine', 87546578, 1984);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Aahil', 'Mcdemott', 51085640, 1982);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Agatha', 'Joseph', 20154275, 1945);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Humera', 'Avila', 75895130, 1968);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Matas', 'Cole', 87456125, 1988);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Sohaib', 'Porte', 48765485, 1977);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Mina', 'Bate', 99856545, 1974);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Myles', 'Feeney', 45618557, 1993);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Lillie', 'Lancaster', 87546485, 1981);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Areeba', 'Vu', 12310544, 1974);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Rikesh', 'Walker', 87545685, 1984);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Taylah', 'Monney', 24163548, 1972);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Marjorie', 'Connor', 52356304, 1966);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Jia', 'Fisher', 78545564, 1987);
INSERT INTO customer(first_name, surname, passport_number, year_of_birth) VALUES ('Gracie', 'Hutchinson', 54654864, 1967);
INSERT INTO customer(first_name, middle_name, surname, passport_number, year_of_birth) VALUES ('Martyna', 'Molie', 'Lord', 16546887, 1971);

--cars
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Audi', 'RS6', 2015, 8900);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('BMW', 'M3', 2020, 15500);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Škoda', 'Superb', 2021, 9000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Saab', '900', 1999, 3500);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Volvo', 'XC90', 2015, 13500);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Volkswagen', 'Arteon', 2007, 7900);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Porsche', 'Taycan 4S', 2019, 15000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Ferrari', 'LaFerrari', 2018, 20000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Chevrolet', 'Camaro', 2017, 7500);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Range Rover', 'Velar', 2019, 15000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Nissan', 'GT-R', 2015, 12600);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Lexus', 'LC500', 2020, 13000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Lada', 'Niva', 1993, 2900);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Lexus', 'RX-450h', 2014, 7800);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('McLaren', 'P1', 2013, 19000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Koenigsegg', 'Regera', 2018, 22000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Bugatti', 'Chiron', 2020, 21000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Maybach', 'GLS650', 2021, 25000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Mercedes', 'AMG-GT63s', 2019, 18000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Porsche', '911', 2015, 12000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Rolls-Royce', 'Cullinan', 2021, 26999);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Cupra', 'Formentor', 2020, 8500);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Tesla', 'Model X', 2018, 11000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Nissan', 'Juke', 2013, 6500);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Cadillac', 'Escalade', 2021, 13500);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Jaguar', 'F-Type', 2019, 9900);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Ford', 'Mustang Mach-E', 2021, 11000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Ford', 'Mustang GT', 2017, 8000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Koenigsegg', 'Agera R', 2016, 18000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Mercedes', 'AMG-E53-Estate', 2018, 10000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Volkswagen', 'Golf-GTI', 2015, 7000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Tesla', 'Model S', 2021, 12900);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Pagani', 'Huayra', 2013, 17700);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Audi', 'E-Tron GT', 2020, 13333);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Dodge', 'Charger', 2017, 8900);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Alfa Romeo', 'Stelvio', 2018, 10000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('BMW', 'M8 Competition', 2021, 13000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Ferrari', '458 Italia', 2012, 15000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Lotus', 'Elise', 2017, 10400);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Škoda', 'Octavia RS', 2016, 7900);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Mercedes', 'EQS', 2021, 14560);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Mercedes', 'S400d', 2020, 11600);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Mercedes', 'AMG-A45s', 2015, 8900);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('BMW', '440i', 2016, 8500);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Volvo', 'S60', 2019, 9900);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Bentley', 'Continental GT', 2018, 13400);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Bentley', 'Bentayga', 2021, 14900);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Rolls-Royce', 'Ghost', 2018, 18900);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('BMW', 'X7', 2019, 12000);
INSERT INTO car(car_brand, car_model, car_year, car_price) VALUES ('Dodge', 'Challanger', 2015, 9900);

--contacts
INSERT INTO contact(contact_type, contact_address, customer_id) VALUES ('tel', '+420 654 456 784', (SELECT customer_id FROM customer WHERE passport_number = 54652656));
INSERT INTO contact(contact_type, contact_address, customer_id) VALUES ('tel', '+420 684 512 345', (SELECT customer_id FROM customer WHERE passport_number = 45456424));
INSERT INTO contact(contact_type, contact_address, customer_id) VALUES ('e-mail', '54sdf8@email.com', 42);
INSERT INTO contact(contact_type, contact_address, customer_id) VALUES ('fax', '+452 548 546 128', 18);
INSERT INTO contact(contact_type, contact_address, customer_id) VALUES ('e-mail', 'dsfasd@gmail.com', 42);

--equipment
INSERT INTO equipment(equipment_type, customer_id) VALUES ('baby seat', 21);
INSERT INTO equipment(equipment_type, customer_id) VALUES ('tow bar', 2);
INSERT INTO equipment(equipment_type, customer_id) VALUES ('ski box', 1);
INSERT INTO equipment(equipment_type, customer_id) VALUES ('bike rack', 44);
INSERT INTO equipment(equipment_type, customer_id) VALUES ('baby seat', 17);

--passengers
INSERT INTO passenger(passenger_name, passenger_middle_name, passenger_surname, passenger_birth, customer_id) VALUES ('Leonardo', 'Di', 'Caprio', '1975/11/11', 2);
INSERT INTO passenger(passenger_name, passenger_middle_name, passenger_surname, passenger_birth, customer_id) VALUES ('Max', 'Super', 'Verstappen', '1997/9/30', 3);
INSERT INTO passenger(passenger_name, passenger_middle_name, passenger_surname, passenger_birth, customer_id) VALUES ('Jan', 'Amos', 'Komenský', '1592/3/28', 1);
INSERT INTO passenger(passenger_name, passenger_surname, passenger_birth, customer_id) VALUES ('River', 'Smith', '1998/7/4', 7);
INSERT INTO passenger(passenger_name, passenger_surname, passenger_birth, customer_id) VALUES ('Juan', 'Valdéz', '1958/9/11', 50);
INSERT INTO passenger(passenger_name, passenger_surname, passenger_birth, customer_id) VALUES ('Pablo', 'Neruda', '1904/7/12', 41);
INSERT INTO passenger(passenger_name, passenger_surname, passenger_birth, customer_id) VALUES ('Kazimír', 'Smrček', '1995/3/21', 36);
INSERT INTO passenger(passenger_name, passenger_surname, passenger_birth, customer_id) VALUES ('Křesomysl', 'Černovous', '1957/1/31', 27);
INSERT INTO passenger(passenger_name, passenger_surname, passenger_birth, customer_id) VALUES ('Mečislav', 'Žižka', '2009/12/3', 15);
INSERT INTO passenger(passenger_name, passenger_surname, passenger_birth, customer_id) VALUES ('Cecil', 'Cejpek', '2014/8/14', 48);

--rent_info
INSERT INTO rent_info(start_date, end_date, usage, note, car_id) VALUES ('2020/10/23', '2020/11/13', 'personal', 'returns without any gasoline', 19);
INSERT INTO rent_info(start_date, end_date, usage, car_id) VALUES ('2020/12/28', '2021/2/1', 'personal', 31);
INSERT INTO rent_info(start_date, end_date, usage, note, car_id) VALUES ('2021/3/15', '2021/5/13', 'personal', 'returns with full tank', 34);
INSERT INTO rent_info(start_date, end_date, usage, car_id) VALUES ('2019/6/18', '2020/7/8', 'work', 13);
INSERT INTO rent_info(start_date, end_date, usage, car_id) VALUES ('2021/4/16', '2021/12/24', 'personal', 48);

--foreign keys rent_info
INSERT INTO customer_has_rent(customer_id, rent_id) VALUES (1, 1);
INSERT INTO customer_has_rent(customer_id, rent_id) VALUES (2, 3);
INSERT INTO customer_has_rent(customer_id, rent_id) VALUES (3, 2);
INSERT INTO customer_has_rent(customer_id, rent_id) VALUES (29, 5);
INSERT INTO customer_has_rent(customer_id, rent_id) VALUES (35, 4);

--licence
INSERT INTO license(license_type) VALUES ('A');
INSERT INTO license(license_type) VALUES ('B');
INSERT INTO license(license_type) VALUES ('B-automat');
INSERT INTO license(license_type) VALUES ('C');
INSERT INTO license(license_type) VALUES ('T');

INSERT INTO customer_has_license(customer_id, license_id) VALUES (1, 2);
INSERT INTO customer_has_license(customer_id, license_id) VALUES (2, 2);
INSERT INTO customer_has_license(customer_id, license_id) VALUES (3, 5);
INSERT INTO customer_has_license(customer_id, license_id) VALUES (29, 1);
INSERT INTO customer_has_license(customer_id, license_id) VALUES (35, 3);

--health
INSERT INTO health(body_disfunction) VALUES ('one leg');
INSERT INTO health(mental_disfunction) VALUES ('dementia');
INSERT INTO health(mental_disfunction) VALUES ('bipolar disorder');
INSERT INTO health(body_disfunction, mental_disfunction) VALUES ('no arms', 'autism');
INSERT INTO health(mental_disfunction) VALUES ('depression');

INSERT INTO customer_has_health(customer_id, health_id) VALUES(6, 1);
INSERT INTO customer_has_health(customer_id, health_id) VALUES(45, 2);
INSERT INTO customer_has_health(customer_id, health_id) VALUES(30, 3);
INSERT INTO customer_has_health(customer_id, health_id) VALUES(24, 4);
INSERT INTO customer_has_health(customer_id, health_id) VALUES(16, 5);

INSERT INTO address(city, street, house_number, zip) VALUES ('Třeboň', 'U Francouzů', 1125, 37901);
INSERT INTO address(city, street, house_number, zip) VALUES ('Hlušovice', 'Akátová', 187, 78314);
INSERT INTO address(city, house_number, zip) VALUES ('Bratčice', 235, 66467);
INSERT INTO address(city, street, house_number, zip) VALUES ('Plzeň', 'Plachého', 32, 30100);
INSERT INTO address(city, street, house_number, zip) VALUES ('Brno', 'Technická', 12, 61200);

INSERT INTO customer_has_address(address_type, customer_id, address_id) VALUES ('Home address', 1, 2);
INSERT INTO customer_has_address(address_type, customer_id, address_id) VALUES ('Home address', 3, 3);
INSERT INTO customer_has_address(address_type, customer_id, address_id) VALUES ('Home address', 2, 1);
INSERT INTO customer_has_address(address_type, customer_id, address_id) VALUES ('Work address', 35, 5);
INSERT INTO customer_has_address(address_type, customer_id, address_id) VALUES ('Temporary address', 29, 4);
