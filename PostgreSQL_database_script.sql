DROP TABLE customer CASCADE;
DROP TABLE address CASCADE;
DROP TABLE customer_has_address CASCADE;
DROP TABLE contact CASCADE;
DROP TABLE car CASCADE;
DROP TABLE rent_info CASCADE;
DROP TABLE customer_has_rent CASCADE;
DROP TABLE license CASCADE;
DROP TABLE passenger CASCADE;
DROP TABLE customer_has_license CASCADE;
DROP TABLE health CASCADE;



CREATE TABLE IF NOT EXISTS "customer"(
  "customer_id" SERIAL PRIMARY KEY NOT NULL,
  "first_name" VARCHAR(45) NOT NULL,
  "middle_name" VARCHAR(45) NULL,
  "surname" VARCHAR(45) NOT NULL,
  "passport_number" INTEGER NOT NULL,
  "year_of_birth" INTEGER NOT NULL
	);

CREATE TABLE IF NOT EXISTS "address" (
  "address_id" SERIAL PRIMARY KEY NOT NULL,
  "city" VARCHAR(45) NOT NULL,
  "street" VARCHAR(45) NULL,
  "house_number" VARCHAR(45) NOT NULL,
  "zip" VARCHAR(45) NOT NULL
	);

CREATE TABLE IF NOT EXISTS "customer_has_address"(
	"address_type" VARCHAR(45) NOT NULL,
	"customer_id" INTEGER NOT NULL,
	"address_id" INTEGER NOT NULL,
	FOREIGN KEY ("customer_id") REFERENCES "customer",
	FOREIGN KEY ("address_id") REFERENCES "address"
	);

CREATE TABLE IF NOT EXISTS "contact"(
	"contact_id" SERIAL PRIMARY KEY NOT NULL,
	"contact_type" VARCHAR(45) NOT NULL,
	"contact_address" VARCHAR(45) NOT NULL,
	"customer_id" INTEGER NOT NULL,
	FOREIGN KEY ("customer_id") REFERENCES "customer"
	);

CREATE TABLE IF NOT EXISTS "car"(
	"car_id" SERIAL PRIMARY KEY NOT NULL,
	"car_brand" VARCHAR(45) NOT NULL,
	"car_model" VARCHAR(45) NOT NULL,
	"car_year" INTEGER NOT NULL,
	"car_price" INTEGER NOT NULL
	);

CREATE TABLE IF NOT EXISTS "rent_info"(
	"rent_id" SERIAL PRIMARY KEY NOT NULL,
	"start_date" DATE NOT NULL,
	"end_date" DATE NOT NULL,
	"usage" VARCHAR(45) NOT NULL,
	"note" VARCHAR(45) NULL,
	"car_id" INTEGER NOT NULL,
	FOREIGN KEY ("car_id") REFERENCES "car"
	);

CREATE TABLE IF NOT EXISTS "customer_has_rent"(
	"customer_id" INTEGER NOT NULL,
	"rent_id" INTEGER NOT NULL,
	FOREIGN KEY ("customer_id") REFERENCES "customer",
	FOREIGN KEY ("rent_id") REFERENCES "rent_info"
	);
	
CREATE TABLE IF NOT EXISTS "passenger"(
	"passenger_id" SERIAL PRIMARY KEY NOT NULL,
	"passenger_name" VARCHAR(45) NOT NULL,
	"passenger_middle_name" VARCHAR(45) NULL,
	"passenger_surname" VARCHAR(45) NOT NULL,
	"passenger_birth" DATE NULL,
	"customer_id" INTEGER NOT NULL,
	FOREIGN KEY ("customer_id") REFERENCES "customer"
	);

CREATE TABLE IF NOT EXISTS "license"(
	"license_id" SERIAL PRIMARY KEY NOT NULL,
	"license_type" VARCHAR(45) NOT NULL
	);

CREATE TABLE IF NOT EXISTS "customer_has_license"(
	"license_id" INTEGER NOT NULL,
	"customer_id" INTEGER NOT NULL,
	FOREIGN KEY ("license_id") REFERENCES "license",
	FOREIGN KEY ("customer_id") REFERENCES "customer"
	);

CREATE TABLE IF NOT EXISTS "equipment"(
	"equipment" SERIAL PRIMARY KEY NOT NULL,
	"equipment_type" VARCHAR(45) NOT NULL,
	"customer_id" INTEGER NOT NULL,
	FOREIGN KEY ("customer_id") REFERENCES "customer"
	);

CREATE TABLE IF NOT EXISTS "health"(
	"health_id" SERIAL PRIMARY KEY NOT NULL,
	"body_disfunction" VARCHAR(45) NULL,
	"mental_disfunction" VARCHAR(45) NULL
	);

CREATE TABLE IF NOT EXISTS "customer_has_health"(
	"customer_id" INTEGER NOT NULL,
	"health_id" INTEGER NOT NULL,
	FOREIGN KEY ("customer_id") REFERENCES "customer",
	FOREIGN KEY ("health_id") REFERENCES "health"
	);